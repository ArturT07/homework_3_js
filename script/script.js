// 1 завдання

let userInput = prompt("Введіть число:");
while (isNaN(userInput) || (userInput == null) || (userInput === "")) {
    userInput = prompt("Введіть число")
}
let num = parseInt(userInput);

if (!isNaN(num)) {
    console.log("Числа, кратні 5, у діапазоні від 1 до", num + ":");
let res = ""
    let hasMultiples = false;

    for (let i = 1; i <= num; i++) {
        if (i % 5 === 0) {
            console.log(i);
            hasMultiples = true;
        }
    }

    if (!hasMultiples) {
        console.log("Sorry, no numbers");
    }
} else {
    console.log("Введено некоректне число");
}




// 2 завдання
let m, n;

while (true) {
    m = parseInt(prompt("Введіть число m:"));
    n = parseInt(prompt("Введіть число n:"));

    if (isNaN(m) || isNaN(n)) {
        console.log("Помилка: Обидва числа повинні бути числами. Будь ласка, спробуйте ще раз.");
        continue;
    }

    if (m >= n) {
        console.log("Помилка: n повинне бути більше за m. Будь ласка, спробуйте ще раз.");
        continue;
    }

    break;
}

console.log(`Прості числа від ${m} до ${n}:`);

for (let i = m; i < n; i++) {
    let isPrime = true;

    if (i < 2) {
        isPrime = false;
    }

    for (let x = 2; x <= Math.sqrt(i); x++) {
        if (i % x === 0) {
            isPrime = false;
            break;
        }
    }

    if (isPrime) {
        console.log(i);
    }
}
